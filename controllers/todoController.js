var bodyParser = require('body-parser');
var mongoose = require('mongoose');

setTimeout(function() {
mongoose.connect('mongodb://localhost:27017/todo' ,  {useNewUrlParser: true});
} , 6000);
var todoSchema = new mongoose.Schema({
    item: String
});

var Todo = mongoose.model('Todo' , todoSchema );  
// var itemOne = Todo({item : 'buy folwers'}).save(function(err){
//     if(err) throw err;
//     console.log(' item saved'); 
// });


// var data = [{item:'get milk'} , {item:'walk dog'}];
var urlEncodeBodyParser = bodyParser.urlencoded({extended : false});

module.exports = function(app){

    app.get('/todo',function(req,res){
        Todo.find({} , function(err , data){
            if(err) throw err;
            res.render('todoList/add',{todos:data});
        });
        
    });

    app.post('/todo',urlEncodeBodyParser , function(req,res){
        var newItem = Todo(req.body).save(function(err , data){
                if(err) throw err;
                res.json(data); 
            });
        // data.push(req.body);
        // res.json(data); 
    });


    app.delete('/todo/:item',function(req,res){
        Todo.find({item: req.params.item.replace(/\-/g, " ")}).deleteOne(function(err,data){
            if(err) throw err;
            res.json(data); 
        });
        // data = data.filter(function(todo){
        //     return todo.item.replace(/ /g, '-') !== req.params.item;
        // });
        // res.json(data);
    });

};