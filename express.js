var express = require('express');
var bodyParser = require('body-parser');
var partials  = require('express-partials');
var todoController = require('./controllers/todoController');
var app = express();
// "use strict";
// const nodemailer = require("nodemailer");
//parse post request 
var urlEncodeBodyParser = bodyParser.urlencoded({extended : false});

app.set('view engine' , 'ejs');

//middleware
app.use('/public',express.static('public'));
app.use(partials());


app.get('/',function(req,res){
    res.send('Hi from express');
});
app.get('/home',function(req,res){
    res.render('home');
});

app.get('/profile/:id',function(req,res){
    res.send('id = ' + req.params.id);
});

app.get('/myprofile/:name',function(req,res){
    res.render('profile',{ person : req.params.name} );
});

app.post('/signin',urlEncodeBodyParser,function(req,res){
    // main();
    if (!req.body) return res.sendStatus(400)
    res.render('profile',{ person : req.body.username} );
    // res.send('welcome, ' + req.body.username);
});

todoController(app);

// async..await is not allowed in global scope, must use a wrapper
// async function main(){

//   // Generate test SMTP service account from ethereal.email
//   // Only needed if you don't have a real mail account for testing
//   let testAccount = await nodemailer.createTestAccount();

//   // create reusable transporter object using the default SMTP transport
//   let transporter = nodemailer.createTransport({
//     host: "smtp.ethereal.email",
//     port: 587,
//     secure: false, // true for 465, false for other ports
//     auth: {
//       user: testAccount.user, // generated ethereal user
//       pass: testAccount.pass // generated ethereal password
//     }
//   });

//   // send mail with defined transport object
//   let info = await transporter.sendMail({
//     from: '"Fred Foo 👻" <foo@example.com>', // sender address
//     to: "saramagdyahmed6@gmail.com", // list of receivers
//     subject: "Hello ✔", // Subject line
//     text: "Hello world?", // plain text body
//     html: "<b>Hello world?</b>" // html body
//   });

//   console.log("Message sent: %s", info.messageId);
//   // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

//   // Preview only available when sending through an Ethereal account
//   console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
//   // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
// }

// main().catch(console.error);
app.listen(3000);