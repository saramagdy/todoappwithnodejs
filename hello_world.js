const http = require('http');
const fs = require('fs');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
  // res.statusCode = 200;
  // res.setHeader('Content-Type', 'text/plain');
  console.log('request was made on url ' + req.url);
  // res.writeHead(200 , {'Content-Type': 'text/plain'});

  // //read from txt file then load in the page 
  // var myReadStream = fs.createReadStream(__dirname + '/sql.txt' , 'utf8');
  // res.writeHead(200 , {'Content-Type': 'text/html'});

  // //read from html file then load in the page 
  // var myReadStream = fs.createReadStream(__dirname + '/header.html' , 'utf8');
  // myReadStream.pipe(res);

  //write json
  res.writeHead(200 , {'Content-Type': 'application/json'});
  var myObj ={
    "name" : 'sara' ,
    "age" : 23
  };
  res.end(JSON.stringify(myObj));
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});